#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>


#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define    NB_FOIS 512

int main (int argc, char **argv)
{
 complexe_float_t c1 = {1.0, 2.0} ;
 complexe_float_t c2 = {3.0, 6.0} ;
 complexe_float_t c3 ;

 complexe_double_t cd1 = {10.0, 7.0};
 complexe_double_t cd2 = {25.0, 32.0};
 complexe_double_t cd3 ;

 struct timeval start, end ;
 
 int i ;

 init_flop_micro () ;
 
 //ADDITION SIMPLE

 c3 = add_complexe_float (c1, c2) ;
 cd3 = add_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

 //MULTIPLICATION SIMPLE

 c3 = mult_complexe_float (c1, c2) ;
 cd3 = mult_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

 //DIVISION SIMPLE

 c3 = div_complexe_float (c1, c2) ;
 cd3 = div_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

//ADDITION MULTIPLE : pour tester l'efficacité
//FLOAT

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c3 = add_complexe_float (c1, c2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle c3.real %f c3.imaginary %f duree %f \n", c3.real, c3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;

//DOUBLE

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd3 = add_complexe_double (cd1, cd2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f duree %f \n", cd3.real, cd3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;

 //MULTIPLICATION MULTIPLE : pour tester l'efficacité
 //FLOAT

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c3 = mult_complexe_float (c1, c2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle c3.real %f c3.imaginary %f duree %f \n", c3.real, c3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;


//DOUBLE

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd3 = mult_complexe_double (cd1, cd2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f duree %f \n", cd3.real, cd3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;


//DIVISION MULTIPLE : pour tester l'efficacité
 //FLOAT

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c3 = div_complexe_float (c1, c2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle c3.real %f c3.imaginary %f duree %f \n", c3.real, c3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;


//DOUBLE

 TOP_MICRO(start) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd3 = div_complexe_double (cd1, cd2) ;
   }

 TOP_MICRO(end) ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f duree %f \n", cd3.real, cd3.imaginary, tdiff_micro (&start, &end)) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS*2, tdiff_micro(&start, &end)) ;
 exit (0) ;

}
