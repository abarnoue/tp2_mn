#include <stdio.h>
#include <x86intrin.h>

#include "../include/mnblas.h"
#include "../include/complexe.h"

#include "flop.h"

#define NB_FOIS 4194

//ON CREE DES POINTEURS SUR DES VECTEURS QUI SERVENT POUR LE CALCUL DES PERFORMANCES

float *vecf1 ;
float *vecf2 ;
float *vecf3 ;

double *vecd1 ;
double *vecd2 ;
double *vecd3 ;

complexe_float_t *vecc1 ;
complexe_float_t *vecc2 ;
complexe_float_t *vecc3 ;

complexe_double_t *vecz1 ;
complexe_double_t *vecz2 ;
complexe_double_t *vecz3 ;

complexe_float_t *gemvc ;
complexe_double_t *gemvz ;

void init()
{
    // ON ATTRIBUE DE L'ESPACE MEMOIRE AUX VECTEURS POUR LES INITIALISER

    vecf1 = malloc(sizeof(float *) * 100) ;
    vecf2 = malloc(sizeof(float *) * 10) ;
    vecf3 = malloc(sizeof(float *) * 10) ;

    vecd1 = malloc(sizeof(double *) * 100) ;
    vecd2 = malloc(sizeof(double *) * 10) ;
    vecd3 = malloc(sizeof(double *) * 10) ;

    vecc1 = malloc(sizeof(complexe_float_t *) * 100) ;
    vecc2 = malloc(sizeof(complexe_float_t *) * 10) ;
    vecc3 = malloc(sizeof(complexe_float_t *) * 10) ;

    vecz1 = malloc(sizeof(float *) * 100 * 2) ;
    vecz2 = malloc(sizeof(float *) * 10 * 2) ;
    vecz3 = malloc(sizeof(float *) * 10 * 2) ;

    gemvc = malloc(sizeof(complexe_float_t *)) ;
    gemvz = malloc(sizeof(double *) * 2) ;
}

int main(int argc, char **argv)
{

    float *fA = malloc(sizeof(float *) * 12) ;
    float *fx = malloc(sizeof(float *) * 4) ;
    float *fy = malloc(sizeof(float *) * 3) ;

    double *dA = malloc(sizeof(double *) * 12) ;
    double *dx = malloc(sizeof(double *) * 4) ;
    double *dy = malloc(sizeof(double *) * 3) ;

    complexe_float_t *cA = malloc(sizeof(complexe_float_t *) * 12) ;
    complexe_float_t *cx = malloc(sizeof(complexe_float_t *) * 4) ;
    complexe_float_t *cy = malloc(sizeof(complexe_float_t *) * 3) ;

    complexe_double_t *zA = malloc(sizeof(double *) * 24) ;
    complexe_double_t *zx = malloc(sizeof(double *) * 8) ;
    complexe_double_t *zy = malloc(sizeof(double *) * 6) ;

    int M = 3 ;
    int N = 4 ;

    complexe_float_t *alphac = malloc(sizeof(complexe_float_t *)) ;
    complexe_float_t *betac = malloc(sizeof(complexe_float_t *)) ;
    complexe_double_t *alphaz = malloc(sizeof(double *) * 2) ;
    complexe_double_t *betaz = malloc(sizeof(double *) * 2) ;

    //FOAT :
    
    //INITIALISATION DES VECTEURS f1 et f2 :


    float initf = 1.0 ;

    for (int i = 0; i < 12; i++)
    {
        fA[i] = initf ;
        initf ++ ;
    }

    initf = 1.0 ;

    for (int i = 0; i < 4; i++)
    {
        fx[i] = initf ;
        initf ++ ;
    }

    initf = 1.0 ;

    for (int i = 0; i < 3; i++)
    {
        fy[i] = initf ;
        initf ++ ;
    }

    float alphaf = 2.0 ;
    float betaf = 2.0 ;

    //on print les deux vecteurs à l'initialisation 

    printf("\n\n\nFLOAT\n\nAvant gemv :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i% 4 == 0)
        {
            printf("\n") ;
        }
        printf("%f, ", fA[i]) ;
    }

    printf("\nVecteur X : ") ;
    for (int i = 0; i < 4; i++)
    {
        printf("%f, ", fx[i]) ;
    }

    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%f, ", fy[i]) ;
    }
    printf("\n") ;

    //on utilise la fonction crée dans swap.c pour échanger deux vecteurs à valeurs flottantes
   
    mncblas_fgemv(101, 111, M, N, alphaf, fA, 0, fx, 1, betaf, fy, 1) ;

    //on print les deux vecteurs après l'inversion

    printf("Après gemv :\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%f, ", fy[i]) ;
    }

    printf("\n") ;

    //DOUBLE :

    //on utilise la même méthode pour les doubles

    double initd = 1.0 ;

    for (int i = 0; i < 12; i++)
    {
        dA[i] = initd ;
        initd ++ ;
    }

    initd = 1.0 ;

    for (int i = 0; i < 4; i++)
    {
        dx[i] = initd ;
        initd++ ;
    }

    initd = 1.0 ;

        for (int i = 0; i < 3; i++)
    {
        dy[i] = initd ;
        initd++ ;
    }

    float alphad = 2.0;
    float betad = 2.0;

    printf("\n\n\nDOUBLE\n\nAvant gemv :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 4 == 0)
        {
            printf("\n");
        }
        printf("%lf, ", dA[i]) ;
    }

    printf("\nVecteur X : ") ;
    for (int i = 0; i < 4; i++)
    {
        printf("%lf, ", dx[i]) ;
    }

    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, ", dy[i]) ;
    }
    printf("\n");

    mncblas_dgemv(101, 111, M, N, alphad, dA, 0, dx, 1, betad, dy, 1) ;

    printf("Après gemv :\nVecteur y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, ", dy[i]) ;
    }
    printf("\n");

    //COMPLEXE :

    complexe_float_t initc = {1.0, 1.0} ;

    for (int i = 0; i < 12; i++)
    {
        cA[i].real = initc.real ;
        cA[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }

    initc.real = 1.0;
    initc.imaginary = 1.0;

    for (int i = 0; i < 4; i++)
    {
        cx[i].real = initc.real ;
        cx[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }

    initc.real = 1.0;
    initc.imaginary = 1.0;

    for (int i = 0; i < 3; i++)
    {
        cy[i].real = initc.real ;
        cy[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }

    alphac[0].real = 2.0;
    betac[0].real = 2.0;

    printf("\n\n\nCOMPLEXE_FLOAT\n\nAvant gemv :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 4 == 0)
        {
            printf("\n");
        }
        printf("%lf %lf       ", cA[i].real, cA[i].imaginary) ;
    }
    printf("\nVecteur X : ") ;
    for (int i = 0; i < 4; i++)
    {
        printf("%lf, %lf      ", cx[i].real, cx[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, %lf      ", cy[i].real, cy[i].imaginary) ;
    }
    printf("\n") ;

    mncblas_cgemv(101, 111, M, N, alphac, cA, 0, cx, 1, betac, cy, 1) ;

    printf("\nAprès gemv :\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, %lf      ", cy[i].real, cy[i].imaginary) ;
    }
    printf("\n") ;

    //COMPLEXE DOUBLE :

    complexe_double_t initz = {1.0, 1.0} ;

    for (int i = 0; i < 12; i++)
    {
        zA[i].real = initz.real ;
        zA[i].imaginary = initz.imaginary;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    initz.real = 1.0;
    initz.imaginary = 1.0;

    for (int i = 0; i < 4; i++)
    {
        zx[i].real = initz.real ;
        zx[i].imaginary = initz.imaginary ;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    initz.real = 1.0;
    initz.imaginary = 1.0;

    for (int i = 0; i < 3; i++)
    {
        zy[i].real = initz.real ;
        zy[i].imaginary = initz.imaginary ;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    alphaz[0].imaginary = 2.0;
    betaz[0].imaginary = 2.0;

    printf("\n\n\nCOMPLEXE_DOUBLE\n\nAvant gemv :\nMAtrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 4 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf     ", zA[i].real, zA[i].imaginary) ;
    }
    printf("\nVecteur X : ") ;
    for (int i = 0; i < 4; i++)
    {
        printf("%lf, %lf     ", zx[i].real, zx[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, %lf     ", zy[i].real, zy[i].imaginary) ;
    }
    printf("\n") ;

    mncblas_zgemv(101, 111, M, N, alphaz, zA, 0, zx, 1, betaz, zy, 1) ;

    printf("Après gemv :\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        printf("%lf, %lf     ", zy[i].real, zy[i].imaginary) ;
    }
    printf("\n") ;

    //On s'intéresse maintenant aux performances des fonctions :

    printf("PERFORMANCES");
    
    unsigned long long start, end;
    int i;
    long long int moyenne = 0;

    init();

    gemvc[0].real = 0.0;
    gemvc[0].imaginary = 0.0;
    gemvz[0].real = 0.0;
    gemvz[0].imaginary = 0.0;

    //FOAT :

    printf("\n\n\nFLOAT\n\n");

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_init(vecf1, 1.0, 100);
        vector_init(vecf2, 2.0, 10);
        vector_init(vecf3, 3.0, 10);

        start = _rdtsc();
        mncblas_fgemv(101, 111, 10, 10, alphaf, vecf1, 0, vecf2, 1, betaf, vecf3, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_fgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("fgemv ", 2 * 100 * NB_FOIS, moyenne);

    //DOUBLE :

    printf("\n\n\nDOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initd(vecd1, 1.0, 100);
        vector_initd(vecd2, 2.0, 10);
        vector_initd(vecd3, 3.0, 10);

        start = _rdtsc();
        mncblas_dgemv(101, 111, 10, 10, alphad, vecd1, 0, vecd2, 1, betad, vecd3, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_dgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("dgemv ", 2 * 100 * NB_FOIS, moyenne);

    //COMPLEXE :

    printf("\n\n\nCOMPLEXE FLOAT\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcf(vecc1, gemvc[0], 100);
        vector_initcf(vecc2, gemvc[0], 10);
        vector_initcf(vecc3, gemvc[0], 10);

        start = _rdtsc();
        mncblas_cgemv(101, 111, 10, 10, alphac, vecc1, 0, vecc2, 1, betac, vecc3, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_cgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("cgemv ", 2 * 100 * NB_FOIS, moyenne);

    //COMPLEXE DOUBLE :

    printf("\n\n\nCOMPLEXE DOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcd(vecz1, gemvz[0], 100);
        vector_initcd(vecz2, gemvz[0], 10);
        vector_initcd(vecz3, gemvz[0], 10);

        start = _rdtsc();
        mncblas_zgemv(101, 111, 10, 10, alphaz, vecz1, 0, vecz2, 1, betaz, vecz3, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_zgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("zgemv ", 2 * 100 * NB_FOIS, moyenne);

    printf("\n");
    
    //ON LIBERE LA MEMOIRE 

    free(fA);
    free(fx);
    free(fy);
    free(dA);
    free(dx);
    free(dy);
    free(cA);
    free(cx);
    free(cy);
    free(zA);
    free(zx);
    free(zy);
    free(alphac);
    free(betac);
    free(alphaz);
    free(betaz);
    free(vecf1);
    free(vecf2);
    free(vecf3);
    free(vecd1);
    free(vecd2);
    free(vecd3);
    free(vecc1);
    free(vecc2);
    free(vecc3);
    free(vecz1);
    free(vecz2);
    free(vecz3);
    free(gemvc);
    free(gemvz);

    exit(0);
}