#include <stdio.h>
#include <x86intrin.h>

#include "../include/mnblas.h"
#include "../include/complexe.h"

#include "flop.h"

#define NB_FOIS 4194

//ON CREE DES POINTEURS SUR DES VECTEURS QUI SERVENT POUR LE CALCUL DES PERFORMANCES

float *vecf1 ;
float *vecf2 ;
float *vecf3 ;

double *vecd1 ;
double *vecd2 ;
double *vecd3 ;

complexe_float_t *vecc1 ;
complexe_float_t *vecc2 ;
complexe_float_t *vecc3 ;

complexe_double_t *vecz1 ;
complexe_double_t *vecz2 ;
complexe_double_t *vecz3 ;

complexe_float_t *gemmc ;
complexe_double_t *gemmz ;

void init()
{
    // ON ATTRIBUE DE L'ESPACE MEMOIRE AUX VECTEURS POUR LES INITIALISER

    vecf1 = malloc(sizeof(float *) * 100) ;
    vecf2 = malloc(sizeof(float *) * 100) ;
    vecf3 = malloc(sizeof(float *) * 100) ;

    vecd1 = malloc(sizeof(double *) * 100) ;
    vecd2 = malloc(sizeof(double *) * 100) ;
    vecd3 = malloc(sizeof(double *) * 100) ;

    vecc1 = malloc(sizeof(complexe_float_t *) * 100) ;
    vecc2 = malloc(sizeof(complexe_float_t *) * 100) ;
    vecc3 = malloc(sizeof(complexe_float_t *) * 100) ;

    vecz1 = malloc(sizeof(float *) * 100 * 2) ;
    vecz2 = malloc(sizeof(float *) * 100 * 2) ;
    vecz3 = malloc(sizeof(float *) * 100 * 2) ;

    gemmc = malloc(sizeof(complexe_float_t *)) ;
    gemmz = malloc(sizeof(double *) * 2) ;
}

int main(int argc, char **argv)
{

    float *fA = malloc(sizeof(float *) * 12) ;
    float *fB = malloc(sizeof(float *) * 12) ;
    float *fC = malloc(sizeof(float *) * 9) ;

    double *dA = malloc(sizeof(double *) * 12) ;
    double *dB = malloc(sizeof(double *) * 12) ;
    double *dC = malloc(sizeof(double *) * 9) ;

    complexe_float_t *cA = malloc(sizeof(complexe_float_t *) * 12) ;
    complexe_float_t *cB = malloc(sizeof(complexe_float_t *) * 12) ;
    complexe_float_t *cC = malloc(sizeof(complexe_float_t *) * 9) ;

    complexe_double_t *zA = malloc(sizeof(double *) * 24) ;
    complexe_double_t *zB = malloc(sizeof(double *) * 24) ;
    complexe_double_t *zC = malloc(sizeof(double *) * 18) ;

    int M = 3 ;
    int N = 3 ;
    int K = 4 ;

    complexe_float_t *alphac = malloc(sizeof(complexe_float_t *)) ;
    complexe_float_t *betac = malloc(sizeof(complexe_float_t *)) ;
    complexe_double_t *alphaz = malloc(sizeof(double *) * 2) ;
    complexe_double_t *betaz = malloc(sizeof(double *) * 2) ;

    //FOAT :
    
    //INITIALISATION DES VECTEURS f1 et f2 :


    float initf = 1.0 ;

    for (int i = 0; i < 12; i++)
    {
        fA[i] = initf ;
        fB[i] = initf ;
        initf ++ ;
    }

    initf = 1.0 ;

    for (int i = 0; i < 9; i++)
    {
        fC[i] = initf ;
        initf ++ ;
    }


    float alphaf = 2.0 ;
    float betaf = 2.0 ;

    //on print les deux vecteurs à l'initialisation 

    printf("\n\n\nFLOAT\n\nAvant gemm :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i% 4 == 0)
        {
            printf("\n") ;
        }
        printf("%f, ", fA[i]) ;
    }
    printf("\nMatrice B : ");
    for (int i = 0; i < 12; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%f ", fB[i]);
    }
    printf("\nMatrice C : ") ;
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%f, ", fC[i]) ;
    }
    printf("\n") ;

    //on utilise la fonction crée dans swap.c pour échanger deux vecteurs à valeurs flottantes
   
    mncblas_fgemm(101, 111, 111, M, N, K, alphaf, fA, 0, fB, 0, betaf, fC, 0) ;

    //on print les deux vecteurs après l'inversion

    printf("Après gemm :\nMatrice C : ") ;
    for (int i = 0; i < 9; i++)
    {
        printf("%f, ", fC[i]) ;
    }

    printf("\n") ;

    //DOUBLE :

    //on utilise la même méthode pour les doubles

    double initd = 1.0 ;

    for (int i = 0; i < 12; i++)
    {
        dA[i] = initd ;
        dB[i] = initd ;
        initd ++ ;
    }

    initf = 1.0 ;

    for (int i = 0; i < 9; i++)
    {
        dC[i] = initd ;
        initd ++ ;
    }

    float alphad = 2.0;
    float betad = 2.0;

   printf("\n\n\nFLOAT\n\nAvant gemm :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i% 4 == 0)
        {
            printf("\n") ;
        }
        printf("%f, ", dA[i]) ;
    }
    printf("\nMatrice B : ");
    for (int i = 0; i < 12; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%f ", dB[i]);
    }
    printf("\nMatrice C : ") ;
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%f, ", dC[i]) ;
    }
    printf("\n") ;

    mncblas_dgemm(101, 111, 111, M, N, K, alphad, dA, 0, dB, 0, betad, dC, 0) ;

    printf("Après gemm :\nMatrice C : ") ;
    for (int i = 0; i < 9; i++)
    {
        printf("%f, ", dC[i]) ;
    }
    printf("\n");

    //COMPLEXE :

    complexe_float_t initc = {1.0, 1.0} ;

    for (int i = 0; i < 12; i++)
    {
        cA[i].real = initc.real ;
        cA[i].imaginary = initc.imaginary ;
        cB[i].real = initc.real ;
        cB[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }

    initc.real = 1.0;
    initc.imaginary = 1.0;

    for (int i = 0; i < 9; i++)
    {
        cC[i].real = initc.real ;
        cC[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }


    alphac[0].real = 2.0;
    betac[0].real = 2.0;

    printf("\n\n\nCOMPLEXE_FLOAT\n\nAvant gemm :\nMatrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 4 == 0)
        {
            printf("\n");
        }
        printf("%lf %lf       ", cA[i].real, cA[i].imaginary) ;
    }
    printf("\nMatrice B: ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf      ", cB[i].real, cB[i].imaginary) ;
    }
    printf("\nMatrice C : ") ;
    for (int i = 0; i < 3; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf      ", cC[i].real, cC[i].imaginary) ;
    }
    printf("\n") ;

    mncblas_cgemm(101, 111, 111, M, N, K, alphac, cA, 0, cB, 0, betac, cC, 0) ;

    printf("\nAprès gemv :\nVecteur Y : ") ;
    for (int i = 0; i < 3; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf      ", cC[i].real, cC[i].imaginary) ;
    }
    printf("\n") ;

    //COMPLEXE DOUBLE :

    complexe_double_t initz = {1.0, 1.0} ;

    for (int i = 0; i < 12; i++)
    {
        zA[i].real = initz.real ;
        zA[i].imaginary = initz.imaginary;
        zB[i].real = initz.real ;
        zB[i].imaginary = initz.imaginary;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    initz.real = 1.0;
    initz.imaginary = 1.0;

    for (int i = 0; i < 9; i++)
    {
        zC[i].real = initz.real ;
        zC[i].imaginary = initz.imaginary ;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    alphaz[0].imaginary = 2.0;
    betaz[0].imaginary = 2.0;

    printf("\n\n\nCOMPLEXE_DOUBLE\n\nAvant gemm :\nMAtrice A : ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 4 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf     ", zA[i].real, zA[i].imaginary) ;
    }
    printf("\nMatrice B: ") ;
    for (int i = 0; i < 12; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf     ", zB[i].real, zB[i].imaginary) ;
    }
    printf("\nMatrice C: ") ;
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf     ", zC[i].real, zC[i].imaginary) ;
    }
    printf("\n") ;

    mncblas_zgemm(101, 111, 111, M, N, K, alphaz, zA, 0, zB, 0, betaz, zC, 0) ;

    printf("Après gemv :\nMatrice C : ") ;
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0)
        {
            printf("\n");
        }
        printf("%lf, %lf     ", zC[i].real, zC[i].imaginary) ;
    }
    printf("\n") ;

    //On s'intéresse maintenant aux performances des fonctions :

    printf("PERFORMANCES");
    
    unsigned long long start, end;
    int i;
    long long int moyenne = 0;

    init();

    gemmc[0].real = 0.0;
    gemmc[0].imaginary = 0.0;
    gemmz[0].real = 0.0;
    gemmz[0].imaginary = 0.0;

    //FOAT :

    printf("\n\n\nFLOAT\n\n");

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_init(vecf1, 1.0, 100);
        vector_init(vecf2, 2.0, 100);
        vector_init(vecf3, 3.0, 100);

        start = _rdtsc();
        mncblas_fgemm(101, 111, 111, 10, 10, 10, alphaf, vecf1, 0, vecf2, 0, betaf, vecf3, 0);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_fgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("fgemv ", 3 * 100 * NB_FOIS, moyenne);

    //DOUBLE :

    printf("\n\n\nDOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initd(vecd1, 1.0, 100);
        vector_initd(vecd2, 2.0, 100);
        vector_initd(vecd3, 3.0, 100);

        start = _rdtsc();
        mncblas_dgemm(101, 111, 111, 10, 10, 10, alphad, vecd1, 0, vecd2, 0, betad, vecd3, 0);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_dgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("dgemv ", 3 * 100 * NB_FOIS, moyenne);

    //COMPLEXE :

    printf("\n\n\nCOMPLEXE FLOAT\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcf(vecc1, gemmc[0], 100);
        vector_initcf(vecc2, gemmc[0], 100);
        vector_initcf(vecc3, gemmc[0], 100);

        start = _rdtsc();
        mncblas_cgemm(101, 111, 111, 10, 10, 10, alphac, vecc1, 0, vecc2, 0, betac, vecc3, 0);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_cgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("cgemv ", 3 * 100 * NB_FOIS, moyenne);

    //COMPLEXE DOUBLE :

    printf("\n\n\nCOMPLEXE DOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcd(vecz1, gemmz[0], 100);
        vector_initcd(vecz2, gemmz[0], 100);
        vector_initcd(vecz3, gemmz[0], 100);

        start = _rdtsc();
        mncblas_zgemm(101, 111, 111, 10, 10, 10, alphaz, vecz1, 0, vecz2, 0, betaz, vecz3, 0);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_zgemv moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("zgemv ", 3 * 100 * NB_FOIS, moyenne);

    printf("\n");
    
    //ON LIBERE LA MEMOIRE 

    free(fA);
    free(fB);
    free(fC);
    free(dA);
    free(dB);
    free(dC);
    free(cA);
    free(cB);
    free(cC);
    free(zA);
    free(zB);
    free(zC);
    free(alphac);
    free(betac);
    free(alphaz);
    free(betaz);
    free(vecf1);
    free(vecf2);
    free(vecf3);
    free(vecd1);
    free(vecd2);
    free(vecd3);
    free(vecc1);
    free(vecc2);
    free(vecc3);
    free(vecz1);
    free(vecz2);
    free(vecz3);
    free(gemmc);
    free(gemmz);

    exit(0);
}