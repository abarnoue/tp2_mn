#include <stdio.h>
#include <x86intrin.h>

#include "../include/mnblas.h"
#include "../include/complexe.h"

#include "flop.h"

#define NB_FOIS 4194

//ON CREE DES POINTEURS SUR DES VECTEURS QUI SERVENT POUR LE CALCUL DES PERFORMANCES

float *vecf1  ;

double *vecd1 ;

complexe_float_t *vecc1 ;

complexe_double_t *vecz1 ;

complexe_float_t *nrm2c ;
complexe_double_t *nrm2z ;

void init()
{
    // ON ATTRIBUE DE L'ESPACE MEMOIRE AUX VECTEURS POUR LES INITIALISER

    vecf1 = malloc(sizeof(float *) * VECSIZE) ;

    vecd1 = malloc(sizeof(double *) * VECSIZE) ;

    vecc1 = malloc(sizeof(complexe_float_t *) * VECSIZE) ;

    vecz1 = malloc(sizeof(float *) * (VECSIZE * 2)) ;

    nrm2c = malloc(sizeof(complexe_float_t *)) ;
    nrm2z = malloc(sizeof(double *) *2) ;
}

int main(int argc, char **argv)
{

    float *f1 = malloc(sizeof(float *) * 6) ;
    float f ;

    double *d1 = malloc(sizeof(double *) * 6) ;
    double d ;

    complexe_float_t *c1 = malloc(sizeof(complexe_float_t *) * 6) ;

    complexe_double_t *z1 = malloc(sizeof(double *) * 12) ;

    //FOAT :
    
    //INITIALISATION DES VECTEURS f1 et f2 :


    float initf = 1.0 ;

    for (int i = 0; i < 6; i++)
    {
        f1[i] = initf ;
        initf += 2 ;
    }

    //on print le vecteur à l'initialisation 

    printf("\n\n\nFLOAT\n\nAvant nrm2 :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%f, ", f1[i]) ;
    }

    printf("\n") ;

    //on utilise la fonction crée dans iamin.c 
   
    f = mnblas_fnrm2(6, f1, 1) ;

    //on print la somme après asum

    printf("Après nrm2 :\n résultat = %f ", f) ;

    printf("\n") ;

    //DOUBLE :

    //on utilise la même méthode pour les doubles

    double initd = 1.0 ;

    for (int i = 0; i < 6; i++)
    {
        d1[i] = initd ;
        initd+= 2 ;
    }

    printf("\n\n\nDOUBLE\n\nAvant nrm2 :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, ", d1[i]) ;
    }
    printf("\n");

    d = mnblas_dnrm2(6, d1, 1) ;

    printf("Après nrm2 :\n résultat = %f ", d) ;
    printf("\n");

    //COMPLEXE :

    complexe_float_t initc = {1.0, 1.0} ;

    for (int i = 0; i < 6; i++)
    {
        c1[i].real = initc.real ;
        c1[i].imaginary = initc.imaginary ;
        initc.real += 2 ;
        initc.imaginary += 2 ;
    }


    printf("\n\n\nCOMPLEXE_FLOAT\n\nAvant nrm2 :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", c1[i].real, c1[i].imaginary) ;
    }
    printf("\n") ;

    f = mnblas_cnrm2(6, c1, 1) ;

    printf("\nAprès nrm2 :\n résultat = %f ", f) ;
    printf("\n") ;

    //COMPLEXE DOUBLE :

    complexe_double_t initz = {1.0, 1.0} ;


    for (int i = 0; i < 6; i++)
    {
        z1[i].real = initz.real ;
        z1[i].imaginary = initz.imaginary;
        initz.real += 2 ;
        initz.imaginary += 2 ;
    }


    printf("\n\n\nCOMPLEXE_DOUBLE\n\nAvant nrm2 :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", z1[i].real, z1[i].imaginary) ;
    }
    printf("\n") ;

    d = mnblas_znrm2(6, z1, 1) ;

    printf("Après nrm2 :\n résultat = %f ", d) ;

    printf("\n") ;

    //On s'intéresse maintenant aux performances des fonctions :

    printf("PERFORMANCES");
    
    unsigned long long start, end;
    int i;
    long long int moyenne = 0;

    init();

    nrm2c[0].real = 0.0;
    nrm2c[0].imaginary = 0.0;
    nrm2z[0].real = 0.0;
    nrm2z[0].imaginary = 0.0;

    //FOAT :

    printf("\n\n\nFLOAT\n\n");

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_init(vecf1, 1.0, VECSIZE);

        start = _rdtsc();
        mnblas_fnrm2(VECSIZE, vecf1, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_fnrm2 moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("fnrm2 ", 2 * VECSIZE * NB_FOIS, moyenne);

    //DOUBLE :

    printf("\n\n\nDOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initd(vecd1, 1.0, VECSIZE);


        start = _rdtsc();
        mnblas_dnrm2(VECSIZE, vecd1, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_dnrm2 moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("dnrm2 ", 2 * VECSIZE * NB_FOIS, moyenne);

    //COMPLEXE :

    printf("\n\n\nCOMPLEXE FLOAT\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcf(vecc1, nrm2c[0], VECSIZE);

        start = _rdtsc();
        mnblas_cnrm2(VECSIZE, vecc1, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_cnrm2 moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("cnrm2 ", 2 * VECSIZE * NB_FOIS, moyenne);

    //COMPLEXE DOUBLE :

    printf("\n\n\nCOMPLEXE DOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcd(vecz1, nrm2z[0], VECSIZE);

        start = _rdtsc();
        mnblas_znrm2(VECSIZE, vecz1, 1) ;
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_znrm2 moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("znrm2 ", 2 * VECSIZE * NB_FOIS, moyenne);

    printf("\n");
    
    //ON LIBERE LA MEMOIRE 

    free(f1);
    free(d1);
    free(c1);
    free(z1);
    free(vecf1);
    free(vecd1);
    free(vecc1);
    free(vecz1);
    free(nrm2c);
    free(nrm2z);

    exit(0);

    }
