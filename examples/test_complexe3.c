#include <stdio.h>
#include <stdlib.h>
#include "mnblas.h"
#include "complexe2.h"


#define    NB_FOIS        512

#include "flop.h"

int main (int argc, char **argv)
{
 complexe_float_t c1= {1.0, 2.0} ;
 complexe_float_t c2= {3.0, 6.0} ;
 complexe_float_t c3 ;

 complexe_double_t cd1 ={10.0, 7.0};
 complexe_double_t cd2 ={25.0, 32.0};
 complexe_double_t cd3 ;

 unsigned long long int start, end  ;
 int i ;

 init_flop_tsc () ;
 
//ADDITION SIMPLE

 c3 = add_complexe_float (c1, c2) ;
 cd3 = add_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

//MULTIPLICATION SIMPLE

 c3 = mult_complexe_float (c1, c2) ;
 cd3 = mult_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

//DIVISION SIMPLE

 c3 = div_complexe_float (c1, c2) ;
 cd3 = div_complexe_double (cd1, cd2) ;

 printf ("c3.real %f c3.imaginary %f\n", c3.real, c3.imaginary) ;
 printf ("cd3.real %f cd3.imaginary %f\n", cd3.real, cd3.imaginary) ;

//ADDITION MULTIPLE
//FLOAT

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c1 = add_complexe_float (c1, c2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle c3.real %f c3.imaginary %f %Ld cycles \n", c3.real, c3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 
 //DOUBLE

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd1 = add_complexe_double (cd1, cd2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f %Ld cycles \n", cd3.real, cd3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 

 //MULTPIPLICATION MULTIPLE
 //FLOAT

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c1 = mult_complexe_float (c1, c2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle c3.real %f c3.imaginary %f %Ld cycles \n", c3.real, c3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 
 //DOUBLE

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd1 = mult_complexe_double (cd1, cd2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f %Ld cycles \n", cd3.real, cd3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 
 //DIVISION MULTIPLE
 //FLOAT

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     c1 = div_complexe_float (c1, c2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle c3.real %f c3.imaginary %f %Ld cycles \n", c3.real, c3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 
 //DOUBLE

 start = _rdtsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd1 = div_complexe_double (cd1, cd2) ;
   }

 end = _rdtsc () ;

 printf ("apres boucle cd3.real %f cd3.imaginary %f %Ld cycles \n", cd3.real, cd3.imaginary, end-start) ;

 calcul_flop_tsc("calcul complexe nano ", NB_FOIS*2, end-start) ;
 
 exit (0) ;
 
}


