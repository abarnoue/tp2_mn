#include <stdio.h>
#include <x86intrin.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define NB_FOIS 4194

//ON CREE DES POINTEURS SUR DES VECTEURS QUI SERVENT POUR LE CALCUL DES PERFORMANCES

float *vecf1  ;
float *vecf2 ;

double *vecd1 ;
double *vecd2 ;

complexe_float_t *vecc1 ;
complexe_float_t *vecc2 ;

complexe_double_t *vecz1 ;
complexe_double_t *vecz2 ;

complexe_float_t *axpyc ;
complexe_double_t *axpyz ;

void init()
{
    // ON ATTRIBUE DE L'ESPACE MEMOIRE AUX VECTEURS POUR LES INITIALISER

    vecf1 = malloc(sizeof(float *) * VECSIZE) ;
    vecf2 = malloc(sizeof(float *) * VECSIZE) ;

    vecd1 = malloc(sizeof(double *) * VECSIZE) ;
    vecd2 = malloc(sizeof(double *) * VECSIZE) ;

    vecc1 = malloc(sizeof(complexe_float_t *) * VECSIZE) ;
    vecc2 = malloc(sizeof(complexe_float_t *) * VECSIZE) ;

    vecz1 = malloc(sizeof(float *) * (VECSIZE * 2)) ;
    vecz2 = malloc(sizeof(float *) * (VECSIZE * 2)) ;

    axpyc = malloc(sizeof(complexe_float_t *)) ;
    axpyz = malloc(sizeof(double *) *2) ;
}

int main(int argc, char **argv)
{

    float *f1 = malloc(sizeof(float *) * 6) ;
    float *f2 = malloc(sizeof(float *) * 6) ;

    double *d1 = malloc(sizeof(double *) * 6) ;
    double *d2 = malloc(sizeof(double *) * 6) ;

    complexe_float_t *c1 = malloc(sizeof(complexe_float_t *) * 6) ;
    complexe_float_t *c2 = malloc(sizeof(complexe_float_t *) * 6) ;

    complexe_double_t *z1 = malloc(sizeof(double *) * 12) ;
    complexe_double_t *z2 = malloc(sizeof(double *) * 12) ;

    complexe_float_t *alphaf = malloc(sizeof(complexe_float_t*)) ;
    complexe_double_t *alphad = malloc(sizeof(double *) * 2) ;

    //FOAT :
    
    //INITIALISATION DES VECTEURS f1 et f2 :


    float initf = 1.0 ;

    for (int i = 0; i < 6; i++)
    {
        f1[i] = initf ;
        initf += 2 ;
    }

    for (int i = 0; i < 6; i++)
    {
        f2[i] = initf ;
        initf ++ ;
    }

    float alpha1 = 2.0;

    //on print les deux vecteurs à l'initialisation 

    printf("\n\n\nFLOAT\n\nAvant axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%f, ", f1[i]) ;
    }

    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%f, ", f2[i]) ;
    }
    printf("\n") ;

    //on utilise la fonction crée dans axpy.c 
   
    mnblas_saxpy(6, alpha1, f1, 1, f2, 1) ;

    //on print les deux vecteurs après l'inversion

    printf("Après axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%f, ", f1[i]) ;
    }

    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%f, ", f2[i]) ;
    }
    printf("\n") ;

    //DOUBLE :

    //on utilise la même méthode pour les doubles

    double initd = 1.0 ;

    for (int i = 0; i < 6; i++)
    {
        d1[i] = initd ;
        initd+= 2 ;
    }

    for (int i = 0; i < 6; i++)
    {
        d2[i] = initd ;
        initd++ ;
    }

    double alpha2 = 2.0;

    printf("\n\n\nDOUBLE\n\nAvant axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, ", d1[i]) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, ", d2[i]) ;
    }
    printf("\n");

    mnblas_daxpy(6, alpha2, d1, 1, d2, 1) ;

    printf("Après axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, ", d1[i]) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, ", d2[i]) ;
    }
    printf("\n");

    //COMPLEXE :

    complexe_float_t initc = {1.0, 1.0} ;
    alphaf[0].real = 2.0 ;
    alphaf[0].imaginary = 3.0 ;

    for (int i = 0; i < 6; i++)
    {
        c1[i].real = initc.real ;
        c1[i].imaginary = initc.imaginary ;
        initc.real += 2 ;
        initc.imaginary += 2 ;
    }

    for (int i = 0; i < 6; i++)
    {
        c2[i].real = initc.real ;
        c2[i].imaginary = initc.imaginary ;
        initc.real ++ ;
        initc.imaginary ++ ;
    }

    printf("\n\n\nCOMPLEXE_FLOAT\n\nAvant axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", c1[i].real, c1[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", c2[i].real, c2[i].imaginary) ;
    }
    printf("\n") ;

    mnblas_caxpy(6, alphaf, c1, 1, c2, 1) ;

    printf("\nAprès axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", c1[i].real, c1[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", c2[i].real, c2[i].imaginary) ;
    }
    printf("\n") ;

    //COMPLEXE DOUBLE :

    complexe_double_t initz = {1.0, 1.0} ;
    alphad[0].real = 2.0 ;
    alphad[0].imaginary = 3.0 ; 

    for (int i = 0; i < 6; i++)
    {
        z1[i].real = initz.real ;
        z1[i].imaginary = initz.imaginary;
        initz.real += 2 ;
        initz.imaginary += 2 ;
    }

    for (int i = 0; i < 6; i++)
    {
        z2[i].real = initz.real ;
        z2[i].imaginary = initz.imaginary ;
        initz.real ++ ;
        initz.imaginary ++ ;
    }

    printf("\n\n\nCOMPLEXE_DOUBLE\n\nAvant axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", z1[i].real, z1[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", z2[i].real, z2[i].imaginary) ;
    }
    printf("\n") ;

    mnblas_zaxpy(6, alphad, z1, 1, z2, 1) ;

    printf("Après axpy :\nVecteur X : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", z1[i].real, z1[i].imaginary) ;
    }
    printf("\nVecteur Y : ") ;
    for (int i = 0; i < 6; i++)
    {
        printf("%lf, %lf; ", z2[i].real, z2[i].imaginary) ;
    }
    printf("\n") ;

    //On s'intéresse maintenant aux performances des fonctions :

    printf("PERFORMANCES");
    
    unsigned long long start, end;
    int i;
    long long int moyenne = 0;

    init();

    axpyc[0].real = 0.0;
    axpyc[0].imaginary = 0.0;
    axpyz[0].real = 0.0;
    axpyz[0].imaginary = 0.0;

    //FOAT :

    printf("\n\n\nFLOAT\n\n");

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_init(vecf1, 1.0, VECSIZE);
        vector_init(vecf2, 2.0, VECSIZE);

        start = _rdtsc();
        mnblas_saxpy(VECSIZE, 2.0, vecf1, 1, vecf2, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_saxpy moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("saxpy ", 2 * VECSIZE * NB_FOIS, moyenne);

    //DOUBLE :

    printf("\n\n\nDOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initd(vecd1, 1.0, VECSIZE);
        vector_initd(vecd2, 2.0, VECSIZE);

        start = _rdtsc();
        mnblas_daxpy(VECSIZE, 2.0, vecd1, 1, vecd2, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_daxpy moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("daxpy ", 2 * VECSIZE * NB_FOIS, moyenne);

    //COMPLEXE :

    printf("\n\n\nCOMPLEXE FLOAT\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcf(vecc1, axpyc[0], VECSIZE);
        vector_initcf(vecc2, axpyc[0], VECSIZE);

        start = _rdtsc();
        mnblas_caxpy(VECSIZE, alphaf, vecc1, 1, vecc2, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_caxpy moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("caxpy ", 2 * VECSIZE * NB_FOIS, moyenne);

    //COMPLEXE DOUBLE :

    printf("\n\n\nCOMPLEXE DOUBLE\n\n");

    moyenne = 0;

    init_flop_tsc();

    for (i = 0; i < NB_FOIS; i++)
    {
        vector_initcd(vecz1, axpyz[0], VECSIZE);
        vector_initcd(vecz2, axpyz[0], VECSIZE);

        start = _rdtsc();
        mnblas_zaxpy(VECSIZE, alphad, vecz1, 1, vecz2, 1);
        end = _rdtsc();

        moyenne += end - start;
    }

    printf("mncblas_zaxpy moyenne : nombre de cycles: %Ld \n", moyenne/NB_FOIS);
    calcul_flop_tsc("zaxpy ", 2 * VECSIZE * NB_FOIS, moyenne);

    printf("\n");
    
    //ON LIBERE LA MEMOIRE 

    free(f1);
    free(f2);
    free(d1);
    free(d2);
    free(c1);
    free(c2);
    free(z1);
    free(z2);
    free(vecf1);
    free(vecf2);
    free(vecd1);
    free(vecd2);
    free(vecc1);
    free(vecc2);
    free(vecz1);
    free(vecz2);
    free(axpyc);
    free(axpyz);

    exit(0);
}